import AppBar from '@material-ui/core/AppBar';
import {withStyles} from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import * as React from 'react';
import { Link } from 'react-router-dom';
import { headerStyles, IHeaderProps } from 'src/types/propsTypes/IHeaderProps';

function Header(props: IHeaderProps) {
  const { classes } = props;

  return (
    <div className={classes.root}>
      <AppBar position="fixed">
        <Toolbar>
          <Link className={classes.link} to="/">Главная</Link>
        </Toolbar>
      </AppBar>
    </div>
  );
}

export default withStyles(headerStyles)(Header);