import {Paper, Table, TableCell, TableHead, TableRow, Typography, withStyles } from '@material-ui/core';
import * as React from 'react';
import { IUserPostListViewProps, userPostListViewStyles } from 'src/types/propsTypes/IUserPostListViewProps';
import UserPostList from './UserPostList';

class UserPostListView extends React.Component<IUserPostListViewProps> {

    public render() {
        const {classes} = this.props;

        return (
            <Paper className={classes.root}>
                <Typography component="h6" variant="h6">
                    Список постов ({this.props.userStore.postCount})
                </Typography>
                <Table className={classes.table}>
                    <TableHead>
                        <TableRow>
                            <TableCell>ID</TableCell>
                            <TableCell>Заголовок</TableCell>
                            <TableCell>Содержимое</TableCell>
                        </TableRow>
                    </TableHead>
                    <UserPostList
                        deleteStatus={this.props.deleteStatus}
                        userStore={this.props.userStore} />
                </Table>
            </Paper>
        );
    }
}

export default withStyles(userPostListViewStyles)(UserPostListView)