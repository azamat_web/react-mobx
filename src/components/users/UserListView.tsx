import {TableBody, withStyles } from '@material-ui/core';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import * as React from 'react';
import history from 'src/history';
import { IUserListViewProps, userListViewstyles } from 'src/types/propsTypes/IUserListViewProps';
import { IUser } from 'src/types/userTypes/IUser';

class UserListView extends React.Component<IUserListViewProps> {

    public userHandler = (id: number) => () => {
        history.push(`/user/${id}`);
    }

    public render() {
        const {classes} = this.props;
        return (
            <TableBody>
                {this.props.users.map((user: IUser, index: number) =>
                    <TableRow classes={{ 
                        root: classes.row}} 
                        key={index}
                        onClick={this.userHandler(user.id)}>
                        <TableCell component="th" scope="row">
                            {user.id}
                        </TableCell>
                        <TableCell>{user.name}</TableCell>
                        <TableCell >{user.email}</TableCell>
                        <TableCell >{user.phone}</TableCell>
                        <TableCell >{user.company.name}</TableCell>
                    </TableRow>
                )}
            </TableBody>
        );
    }
}

export default withStyles(userListViewstyles)(UserListView);