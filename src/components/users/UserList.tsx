import {InputAdornment, TextField, Typography} from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import {withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Search from '@material-ui/icons/Search';
import {inject, observer } from 'mobx-react';
import * as React from 'react';
import { IUserListProps, userListStyles } from 'src/types/propsTypes/IUserListProps';
import { IUserListState } from 'src/types/stateTypes/IUserListState';
import UserListView from './UserListView';

@inject('UserStore')
@observer
class UserList extends React.Component<IUserListProps, IUserListState> {

    public readonly userStore = this.props.UserStore;

    constructor(props: IUserListProps) {
        super(props);
        this.state = {
            search: ''
        }
    }

    public componentDidMount = () => {
        this.userStore.loadUsers();
    }

    public searchHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({
            search: event.target.value.toLowerCase(),
        });
    }

    public render() {
        const {classes} = this.props;
        const filteredUsers = this.userStore.users.filter((user) => {
            return user.name.toLowerCase().indexOf(this.state.search) !== -1;
        });
       
        return (
             <Paper className={classes.root}>
                <TextField id="filled-search"
                    label="Поиск по имени"
                    type="search"
                    className={classes.textField}
                    margin="normal"
                    variant="filled"
                    onChange={this.searchHandler}
                    InputProps={{
                        startAdornment: (
                          <InputAdornment className={classes.adornment} position="start">
                            <Search />
                          </InputAdornment>
                        ),
                      }}
                />
                <Typography component="h6" variant="h6">
                    Количество пользователей {this.userStore.userCount}
                </Typography>
                <Table className={classes.table}>
                    <TableHead>
                        <TableRow>
                            <TableCell>ID</TableCell>
                            <TableCell>ФИО</TableCell>
                            <TableCell>Email</TableCell>
                            <TableCell>Телефон</TableCell>
                            <TableCell>Компания</TableCell>
                        </TableRow>
                    </TableHead>
                    <UserListView users={filteredUsers} />
                </Table>
            </Paper>
        );
    }
}

export default withStyles(userListStyles)(UserList)