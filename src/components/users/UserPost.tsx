import { Button, Snackbar, withStyles } from '@material-ui/core';
import { inject, observer } from 'mobx-react';
import * as React from 'react';
import { IUserPostProps, userPostStyles } from 'src/types/propsTypes/IUserPostProps';
import { IUserPostState } from 'src/types/stateTypes/IUserPostState';
import { UserDialog } from './UserDialog';
import UserPostListView from './UserPostListView';

@inject('UserStore')
@observer
class UserPost extends React.Component<IUserPostProps, IUserPostState> {

    public readonly userStore = this.props.UserStore;
    public readonly id = this.props.match.params.id;

    constructor(props: IUserPostProps) {
        super(props);

        this.state = {
            isDeleted: false,
            isLoading: false,
            open: false,
        };
    }

    public componentDidMount() {
        if (this.userStore.loadUserByID(this.id)) {
            this.setState({ isLoading: true });
        }
    }

    public handleClickOpen = () => {
        this.setState({ open: true });
    };
    
    public handleClose = () => {
        this.setState({ 
            isDeleted: false,
            open: false,
         });
    };

    public deleteStatus = (deleted: boolean) => {
        if (deleted) {
            this.setState({
                isDeleted: deleted,
             });
        }
    };
   
    public render() {
        const {classes} = this.props;

        if(!this.state.isLoading){
            return ('Loading...');
        }
        return (
        <div className="UserDetail">
            <Button variant="contained" color="primary" onClick={this.handleClickOpen} className={classes.button}>
                Добавить
            </Button>
            <UserDialog
                userStore={this.userStore}
                userId={this.id}
                open={this.state.open}
                handleClose={this.handleClose}
            />
            <Snackbar
                anchorOrigin={{horizontal: 'left',vertical: 'bottom',}}
                open={this.state.isDeleted}
                autoHideDuration={1000}
                onClose={this.handleClose}
                ContentProps={{'aria-describedby': 'message-id'}}
                message={<span id="message-id">Запись Удалена!</span>}
            />
            <UserPostListView
                deleteStatus={this.deleteStatus}
                userStore={this.userStore}
            />
        </div>
        );
    }
}
export default withStyles(userPostStyles)(UserPost)