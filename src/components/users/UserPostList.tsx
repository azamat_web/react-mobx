import { Button, TableBody, withStyles } from '@material-ui/core';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';
import DeleteIcon from '@material-ui/icons/Delete';
import * as React from 'react';
import { IUserPostListProps, userPostListStyles } from 'src/types/propsTypes/IUserPostListProps';
import { IPost } from 'src/types/userTypes/IPost';

class UserPostList extends React.Component<IUserPostListProps> {

    public readonly userStore = this.props.userStore;

    public handleRemove = (id: number) => (index: number) => (event: React.MouseEvent<HTMLElement>): void => {
        if (this.userStore.deletePost(id, index)) {
            this.props.deleteStatus(true);
        }
     };

    public render() {
        const {classes} = this.props;
        return (
            <TableBody>
                    {this.userStore.posts.map((post: IPost, index: number) =>
                    <TableRow key={index}>
                    <TableCell>{post.id}</TableCell>
                    <TableCell>{post.title}</TableCell>
                    <TableCell>{post.body}</TableCell>
                    <TableCell >
                        <Button onClick={this.handleRemove(post.id)(index)} size="small" aria-label="Delete" className={classes.remove}>
                            <DeleteIcon />
                        </Button>
                    </TableCell>
                  </TableRow>
                )}
            </TableBody>
        );
    }
}

export default withStyles(userPostListStyles)(UserPostList);