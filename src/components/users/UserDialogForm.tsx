import { Button, TextField } from '@material-ui/core';
import * as React from 'react';
import { IPost } from 'src/types/userTypes/IPost';
import { IUserStore } from 'src/types/userTypes/IUserStore';


interface IProps {
    userStore: IUserStore;
    userId: number;
    open: boolean;
    handleClose(isOpen: boolean): void
}

interface IState {
    title: string;
    body: string;
    email: string;
    isOpen: boolean;
    userId: number;
}

export class UserDialogForm extends React.Component<IProps, IState> {

    public userStore = this.props.userStore;

    constructor(props: IProps) {
        super(props);

        this.state = {
            body: '',
            email: 'test@gmail.com',
            isOpen: this.props.open,
            title: '',
            userId: 0
        }
    }

    public handleClose = () => {
        this.setState({
            isOpen: false
        });
        this.props.handleClose(this.state.isOpen);
    }

    public handleTitleChange = (event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement | HTMLSelectElement>) => {
        const value = event.target.value;

        this.setState({
            title: value,
        });
    }

    public handleBodyChange = (event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement | HTMLSelectElement>) => {
        const value = event.target.value;

        this.setState({
            body: value,
        });
    }

    public handleSave = async (event: React.FormEvent<HTMLFormElement>) => {
        const post: IPost = {
            body: this.state.body,
            email: this.state.email,
            id: 0,
            title: this.state.title,
            userId: this.state.userId,
        };

        this.userStore.createPost(post);
        this.handleClose();
        event.preventDefault();
    }

    public render() {
        return (
            <form onSubmit={this.handleSave}>
                <TextField
                    autoFocus={true}
                    name="title"
                    required={true}
                    margin="dense"
                    id="title"
                    label="Title"
                    type="text"
                    error={false}
                    fullWidth={true}
                    onChange={this.handleTitleChange}
                />
                <TextField
                    autoFocus={true}
                    required={true}
                    margin="dense"
                    id="body"
                    name="body"
                    label="Body"
                    type="text"
                    multiline={true}
                    error={false}
                    fullWidth={true}
                    onChange={this.handleBodyChange}
                />
                <Button color="primary" type="submit">Сохранить</Button>
                <Button onClick={this.handleClose} color="primary">Закрыть</Button>
            </form>
        );
    }
}