import { Dialog, DialogContent, DialogContentText, DialogTitle } from '@material-ui/core';
import * as React from 'react';
import { IUserDialogProps } from 'src/types/propsTypes/IUserDialogProps';
import { IUserDialogState } from 'src/types/stateTypes/IUserDialogState';
import { UserDialogForm } from './UserDialogForm';

export class UserDialog extends React.Component<IUserDialogProps, IUserDialogState> {

    public userStore = this.props.userStore;

    constructor(props: IUserDialogProps) {
        super(props);

        this.state = {
            body: '',
            email: 'test@gmail.com',
            isOpen: this.props.open,
            title: '',
            userId: 0
        }
    }

    public handleClose = () => {
        this.setState({
            isOpen: false
        });
        this.props.handleClose(this.state.isOpen);
    }

    public render() {
        return (
            <Dialog
                open={this.props.open}
                onClose={this.handleClose}
                aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Добавить пост</DialogTitle>
                <DialogContent>
                <DialogContentText>
                   Список добавленых постов
                </DialogContentText>
                <UserDialogForm
                    open={this.props.open}
                    userId={this.props.userId}
                    handleClose={this.handleClose}
                    userStore={this.userStore} />
            </DialogContent>
          </Dialog>
        );
    }

}