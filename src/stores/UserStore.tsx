import axios, { AxiosResponse } from 'axios';
import {action, computed, observable} from 'mobx';
import Paths from 'src/enum/Paths';
import { IPost } from 'src/types/userTypes/IPost';
import {IUser} from '../types/userTypes/IUser';

class UserStore {

    @observable public users: IUser[] = [];
    @observable public user: IUser;
    @observable public posts: IPost[] = [];


    @computed
    public get userCount(): number {
        return this.users.length;
    }

    @computed
    public get postCount(): number {
        return this.posts.length;
    }

    @action
    public async loadUsers() {
        try {
            const response: AxiosResponse<IUser[]> = await axios.get(`${Paths.api}/users`);
            this.users = response.data;
            
        } catch (error) {
            console.error(error);
        }
    }

    @action
    public loadUserByID = async (id: number) => {
        try {
            const userResponse: AxiosResponse<IUser> = await axios.get(`${Paths.api}/users/${id}`);
            this.user =  await userResponse.data;
            const postsResponse: AxiosResponse<IPost[]> = await axios.get(`${Paths.api}/users/${id}/posts`);
            this.posts = await postsResponse.data;

            return true;

        } catch (error) {
            return false;
        }
    }

    @action
    public deletePost = async (id: number, index: number) => {
        try {
            const response: AxiosResponse<IPost> = await axios.delete(`${Paths.api}/posts/${id}`);
            if (response.status === 200) {
                this.posts.splice(index, 1);
            }
            return true;

        } catch (error) {
            this.posts.splice(index, 1);
            return false;
        }
    }

    @action
    public createPost = async (post: IPost) => {
        try {
            const response: AxiosResponse<IPost> = await axios.post(`${Paths.api}/posts/`, {
                body: post.body,
                email: post.email,
                id: post.id,
                title: post.title,
                userId: post.userId
            });
            this.posts.push(await response.data);

        } catch (error) {
            console.error(error);
        }
    }


}

export default UserStore;