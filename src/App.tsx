import * as React from 'react';
import { Route, Router, Switch } from 'react-router-dom'
import './App.css';
import Header from './components/Header';
import UserList from './components/users/UserList';
import UserPost from './components/users/UserPost';
import history from './history';

class App extends React.Component {
  public render() {
    return (
      <Router history={history}>
        <div className="App">
          <Header />
          <Switch>
            <Route exact={true} path='/' component={UserList}/>
            <Route path='/user/:id' component={UserPost}/>
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
