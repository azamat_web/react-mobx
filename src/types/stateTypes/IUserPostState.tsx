export interface IUserPostState {
    isLoading: boolean;
    isDeleted: boolean;
    open: boolean;
}
