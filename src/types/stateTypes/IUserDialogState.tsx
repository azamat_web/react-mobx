export interface IUserDialogState {
    title: string;
    body: string;
    email: string;
    isOpen: boolean;
    userId: number;
}