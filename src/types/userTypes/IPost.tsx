export interface IPost {
    body: string;
    email: string;
    id: number;
    title: string;
    userId: number;
}