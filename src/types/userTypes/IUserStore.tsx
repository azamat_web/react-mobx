import { IPost } from './IPost';
import { IUser } from './IUser';

export interface IUserStore {
    user: IUser;
    posts: IPost[];
    users: IUser[];
    userCount(): number;
    postCount(): number;
    loadUsers(): void;
    createPost(post: IPost): void;
    deletePost(id: number, index: number): boolean;
    editPost(id: number): void;
    loadUserByID(id: number): boolean;
}