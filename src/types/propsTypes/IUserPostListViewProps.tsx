import { createStyles, Theme } from '@material-ui/core';
import { IUserStore } from '../userTypes/IUserStore';

export const userPostListViewStyles = (theme: Theme) => createStyles({
    root: {
        marginTop: '77px',
        overflowX: 'auto',
        width: '82%',
    },
    table: {
        minWidth: 700,
    },
});

interface IClasses {
    root: string;
    table: string;
}
export interface IUserPostListViewProps {
    userStore: IUserStore;
    classes: IClasses;
    deleteStatus(deleted: boolean): void;
}