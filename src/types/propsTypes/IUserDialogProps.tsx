import { IUserStore } from '../userTypes/IUserStore';

export interface IUserDialogProps {
    userStore: IUserStore;
    userId: number;
    open: boolean;
    handleClose(isOpen: boolean): void
}