import { createStyles, Theme} from '@material-ui/core';
import { IUserStore } from '../userTypes/IUserStore';

export const userPostListStyles = (theme: Theme) => createStyles({
    remove: {
        margin: theme.spacing.unit,
    },
    root: {
      cursor: 'pointer',
    },
});

interface IClasses {
    root: string;
    remove: string;
}
export interface IUserPostListProps {
    userStore: IUserStore;
    classes: IClasses;
    deleteStatus(isDelete: boolean): void,
}