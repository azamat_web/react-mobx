import { createStyles, Theme } from '@material-ui/core';
import { IMatchProps } from '../IMatchProps';
import { IUserStore } from "../userTypes/IUserStore";

export const userPostStyles = (theme: Theme) => createStyles({
    button: {
        left: '86%',
        margin: theme.spacing.unit,
        position: 'absolute',
        top: '10%'

    },
});

interface IClasses {
    button: string;
}

export interface IUserPostProps {
    classes: IClasses;
    UserStore: IUserStore;
    match: IMatchProps;
}