import { createStyles, Theme, WithStyles } from "@material-ui/core";
import { IUserStore } from '../userTypes/IUserStore';

export const userListStyles = (theme: Theme) => createStyles({
    adornment: {
      marginTop: '15px',
    },
    root: {
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
        position: 'absolute',
        top: '6%',
        width: '100%',
      },
      row: {
        cursor: 'pointer',
      },
      table: {
        minWidth: 700,
      },
      textField: {
        marginRight: '75%',
        minWidth: '200px'
      },
     
});

interface IClasses {
    adornment: string;
    table: string;
    root: string;
    row: string;
    textField: string;
}

export interface IUserListProps extends WithStyles<typeof userListStyles> {
    classes: IClasses;
    UserStore: IUserStore;
}