import { createStyles, Theme} from '@material-ui/core';
import { IUser } from '../userTypes/IUser';

export const userListViewstyles = (theme: Theme) => createStyles({
    row: {
        cursor: 'pointer',
    },
});

interface IClasses {
    row: string;
}
export interface IUserListViewProps {
    users: IUser[];
    classes: IClasses;
}