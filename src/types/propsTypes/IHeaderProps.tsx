import { createStyles, Theme } from '@material-ui/core';

export const headerStyles = (theme: Theme) => createStyles({
    button: {
      margin: theme.spacing.unit,
    },
    link: {
      color: '#f1f8e9',
    },
    root: {
      flexGrow: 1,
    },
  });
  
  export interface IHeaderProps {
    classes: {
      root: string;
      button: string;
      link: string;
    }
  }