export interface IMatchProps {
  isExact: boolean;
  params: IParams
  path: string;
  url: string;
}

interface IParams {
  id: number;
}